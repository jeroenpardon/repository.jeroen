# Jeroen's Kodi repository
Beta releases of my skins and other add-ons for Kodi, and add-ons by other authors for testing purposes. Redistribution of those add-ons with permission from the authors.

## License
[![badge-license](
https://img.shields.io/badge/License-CC_BY--NC--SA-brightgreen.svg?style=flat-square
)](http://creativecommons.org/licenses/by-nc-sa/3.0/)

This software is licensed under a [Attribution-NonCommercial-ShareAlike 3.0 Unported license](http://creativecommons.org/licenses/by-nc-sa/3.0/)

excerpt:
"NonCommercial — You may *not* use the material for commercial purposes."

Please note that some content found in this repository may use a different license form than mentioned above. License information is included where applicable.

## Download and install
[![badge-install](https://img.shields.io/badge/Download-Latest-blue.svg?style=flat-square)](https://gitlab.com/jeroenpardon/repository.jeroen/raw/master/repository.jeroen/repository.jeroen-1.0.0.zip) 

After you have downloaded the latest release, please refer to [this installation guide](http://wiki.kodi.tv/index.php?title=HOW-TO:Install_an_Add-on_from_a_zip_file)

## Support
If you like my work and would like to buy me a beer (or coffee), a [donation](https://gitlab.com/jeroenpardon/repository.jeroen/wikis/Support-and-contribute) is greatly appreciated.

[![Image](https://www.paypalobjects.com/images/shared/paypal-logo-129x32.svg "Donate Link") ](http://bit.ly/2nXuTGN "Grid donation button")